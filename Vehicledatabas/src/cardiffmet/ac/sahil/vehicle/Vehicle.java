package cardiffmet.ac.sahil.vehicle;

public class Vehicle {
	
	String registration_number;
	int manufactured_year;
	
	void set_registration_number(String pno)
	{
		registration_number = pno;
	}
	void set_manufactured_year(int year)
	{
		manufactured_year = year;
	}
	
	// comment

	public static void main(String args[])
	{
		System.out.println("This is vehicle tester branch");

		Vehicle vl = new Vehicle();
		vl.set_manufactured_year(2011);
		vl.set_registration_number("GY61 QBC");
		
		System.out.println("Vehicle object created");		
	}
	

	
	
}

